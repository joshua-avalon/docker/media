# [media][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

```sh
docker run -it -v /tmp:/media:rw --rm joshuaavalon/media
```

[docker]: https://hub.docker.com/r/joshuaavalon/media
[docker_pull]: https://img.shields.io/docker/pulls/joshuaavalon/media.svg
[docker_star]: https://img.shields.io/docker/stars/joshuaavalon/media.svg
[docker_size]: https://img.shields.io/microbadger/image-size/joshuaavalon/media.svg
[docker_layer]: https://img.shields.io/microbadger/layers/joshuaavalon/media.svg
[license]: https://gitlab.com/joshua-avalon/docker/media/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/joshua-avalon/docker/media/pipelines
[gitlab_ci]: https://gitlab.com/joshua-avalon/docker/media/badges/master/pipeline.svg
[project]: https://gitlab.com/joshua-avalon/docker/media
