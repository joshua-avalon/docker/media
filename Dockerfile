FROM python:3-alpine

COPY root/ /

RUN chmod +x /usr/local/bin/*

WORKDIR /media

RUN apk add --update --no-cache mkvtoolnix ffmpeg bash && \
    apk add --no-cache --virtual .build-deps git && \
    pip install git+https://github.com/joshuaavalon/pymkv.git && \
    apk del .build-deps

CMD ["/bin/sh"]
